<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
	<link rel="stylesheet" href="css/main.css">
	<style>
        .overlayFormMember{
            display: block;
        }
        .overlayFormMember .popup{
            top: 12%;
            left: 32%;
            transform: translate(0,0);
            height: 400px;
        }
        .popup .wrappers{
            height: 400px;
            overflow: auto;
        }
    </style>
</head>
<body>
    <div class="overlayFormMember js-overlay">
        <div class="popup js-popup-campaign">
            <div class="btnClosse"></div>
            <div class="wrappers">
                <div class="paymentContainer">
                    <h3 class="titlePay">Payment security</h3>
                    <div class="plan">
                        <div class="item">
                            <p class="titlePlan">Product</p>
                            <p class="values valuesRegular">Amount</p>
                        </div>
                        <div class="item">
                            <p class="pricePlan">Subscription</p>
                            <p class="values">
                                <?php if(!empty($_GET['price'])) echo  $_GET['price'];?>€
                            </p>
                        </div>
                        <div class="item">
                            <p class="pariodPlan">TVA</p>
                            <p class="values">
                                <?php if(!empty($_GET['price'])) echo $_GET['price']*20/100;?>€
                            </p>
                        </div>
                        <div class="item">
                            <p class="workoutPlan">Total</p>
                            <p class="values valuesBold">
                                <?php if(!empty($_GET['price'])) echo  $_GET['price'] + $_GET['price']*20/100;?>€
                            </p>
                        </div>
                    </div>
                </div>
                <div class="paymentContainer">
                    <form class="form">
                        <div class="wrapInput">
                            <div class="itemInput">
                                <label for="cardType" class="formLabel">Card type</label>
                                <div class="select">
                                    <select name="" id="cardType">
                                        <option value="">select card type</option>
                                        <option value="Visa">Visa</option>
                                        <option value="Maestro">Maestro</option>
                                        <option value="Mastercard">Mastercard</option>
                                        <option value="American Express">American Express</option>
                                        <option value="Discover Card">Discover Card</option>
                                    </select>
                                </div>
                                <img class="imgCard" id="imgCardType" src="" alt="">
                            </div>
                            <div class="itemInput">
                                <label for="card" class="formLabel">Cart <br>number</label>
                                <input type="text" class="input cardInput" id="card" name="name" placeholder="0000-0000-0000-0000"/>
                            </div>
                            <div class="itemInput">
                                <label for="cardExpiration" class="formLabel">Expiration <br>Date</label>
                                <input type="text" class="input" id="cardExpiration" name="name" placeholder=""/>
                            </div>
                            <div class="itemInput">
                                <label for="nameCard" class="formLabel">Name</label>
                                <input type="text" name="name" class="input" id="nameCard" placeholder="">
                            </div>
                            <div class="itemInput">
                                <label for="cvvCode" class="formLabel">CVV</label>
                                <input type="text" name="name" class="input" id="cvvCode" placeholder="">
                            </div>
                            <div class="wrapBtn">
                                <div class="wrapSubmit">
                                    <input type="submit" class="btnSubmit" value="CONFIRM PAYMENT">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="paymentContainer">
                    <div class="wrapRemember">
                        <p class="textRemember">
                            Your order number is <?php if(!empty($_GET['order_number'])) echo $_GET['order_number']?>.
                            Remember, your subscription is a subscription service.
                            If you do not unsubscribe before the expiry of the trial period, the subscription will
                            be renewed every 30 days in the amount of € <?php if(!empty($_GET['price'])) echo  $_GET['price'] + $_GET['price']*20/100;?>. This campaign complies with the requirements of the European Union.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>